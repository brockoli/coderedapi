package com.brockoli.android.codered.api;
/*
 * Copyright 2014 brockoli Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

/**
 * A serializable object representing a single film item produced by a {@link CodeRedFilmSource}. A
 * film is simple an {@linkplain FilmItem.Builder#contentUri(Uri) image} along with
 * some metadata.
 *
 * <p> To create an instance, use the {@link FilmItem.Builder} class.
 */
public class FilmItem {
	private static final String KEY_CONTENT_URI = "contentUri";
	private static final String KEY_TITLE = "title";
	private static final String KEY_BYLINE = "byline";
	private static final String KEY_TOKEN = "token";
	private static final String KEY_VIEW_INTENT = "viewIntent";
	private static final String KEY_THUMB_URI = "thumbUri";
	private static final String KEY_BRANDING_PHOTO_URI = "brandingPhotoUri";
	private static final String KEY_BRANDING_HIGHLIGHT_BG = "brandingHighlightBG";
	private static final String KEY_BRANDING_HIGHLIGHT_IMAGE_URI = "brandingHighlightImageUri";
	private static final String KEY_BRANDING_TEXT = "brandingText";
	private static final String KEY_BRANDING_TAG_URI = "brandingTag";
	private static final String KEY_TIMESTAMP = "timestamp";

	private Uri mContentUri;
	private Uri mThumbUri;
	private Uri mBrandingPhotoUri;
	private Uri mBrandingHighlightImageUri;
	private Uri mBrandingTagUri;
	private String mTitle;
	private String mByline;
	private String mBrandingHighlightBG;
	private String mBrandingText;
	private String mToken;
	private long mTimestamp = 0;
	private Intent mViewIntent;

	private FilmItem() {
	}

	/**
	 * Returns the films content URI, or null if it doesn't have one.
	 *
	 * @see FilmItem.Builder#contentUri(Uri)
	 */
	public Uri getContentUri() {
		return mContentUri;
	}

	public Uri getThumbUri() {
		return mThumbUri;
	}

	public Uri getBrandingPhotoUri() {
		return mBrandingPhotoUri;
	}
	
	public Uri getBrandingHighlightImageUri() {
		return mBrandingHighlightImageUri;
	}
	
	public Uri getBrandingTag() {
		return mBrandingTagUri;
	}
	
	/**
	 * Returns the films user-visible title, or null if it doesn't have one.
	 *
	 * @see FilmItem.Builder#title(String)
	 */
	public String getTitle() {
		return mTitle;
	}

	/**
	 * Returns the films user-visible byline (e.g. author and date), or null if it doesn't have
	 * one.
	 *
	 * @see FilmItem.Builder#byline(String)
	 */
	public String getByline() {
		return mByline;
	}

	public String getBrandingBackgroundColor() {
		return mBrandingHighlightBG;
	}
	
	public String getBrandingText() {
		return mBrandingText;
	}
	
	public long getTimestamp() {
		return mTimestamp;
	}
	
	/**
	 * Returns the films opaque application-specific identifier, or null if it doesn't have
	 * one.
	 *
	 * @see FilmItem.Builder#token(String)
	 */
	public String getToken() {
		return mToken;
	}

	/**
	 * Returns the activity {@link Intent} that will be started when the user clicks
	 * for more details about the film, or null if the film doesn't have one.
	 *
	 * @see FilmItem.Builder#viewIntent(Intent)
	 */
	public Intent getViewIntent() {
		return mViewIntent;
	}

	/**
	 * A <a href="http://en.wikipedia.org/wiki/Builder_pattern">builder</a>-style, <a
	 * href="http://en.wikipedia.org/wiki/Fluent_interface">fluent interface</a> for creating {@link
	 * FilmItem} objects. Example usage is below
	 *
	 * <pre class="prettyprint">
	 * FilmItem film = new FilmItem.Builder()
	 *         .contentUri(Uri.parse("http://example.com/image.m3u8"))
	 *         .thumbUri(Uri.parse("http://example.com/thumb.png"))
	 *         .title("Example video")
	 *         .byline("Unknown person, c. 1980")
	 *         .brandingTagUri(Uri.parse("http://example.com/some_logo_image.png"))
     *         .brandingPhotoUri(Uri.parse("http://example.com/some_context_relevant_photo.png"))
     *         .brandingHighlightImageUri(Uri.parse("http://example.com/some_banner_image.png"))
     *         .brandingHighlightColor("#333333")
     *         .brandingText("Some context relevant text to overlay highlight banner")
     *         .timestamp(12345678901234)
	 *         .viewIntent(new Intent(Intent.ACTION_VIEW,
	 *                 Uri.parse("http://example.com/imagedetails.html")))
	 *         .build();
	 * </pre>
	 *
	 * The only required field is {@linkplain #contentUri(Uri) the content URI}, but you
	 * should really provide all the metadata, especially title, byline, and view intent.
	 */
	public static class Builder {
		private FilmItem mFilmItem;

		public Builder() {
			mFilmItem = new FilmItem();
		}

        // Uri to some hosted content that we'll launch using Intent ACTION_VIEW
		public Builder contentUri(Uri contentUri) {
			mFilmItem.mContentUri = contentUri;
			return this;
		}

        // Uri to the tile thumbnail image for a video
		public Builder thumbUri(Uri thumbUri) {
			mFilmItem.mThumbUri = thumbUri;
			return this;
		}

        // Uri to a logo that will be overlayed the thumbnail in the upper right hand corner
		public Builder brandingTagUri(Uri brandingTagUri) {
			mFilmItem.mBrandingTagUri = brandingTagUri;
			return this;
		}

        // Uri to a photo thumbnail that will be overlaid on the highlight banner
		public Builder brandingPhotoUri(Uri brandingPhotoUri) {
			mFilmItem.mBrandingPhotoUri = brandingPhotoUri;
			return this;
		}

        // Uri to an image that will be used in the highlight banner (background)
		public Builder brandingHighlightImageUri(Uri brandingHighlightImageUri) {
			mFilmItem.mBrandingHighlightImageUri = brandingHighlightImageUri;
			return this;
		}

		/**
		 * Sets the films user-visible title.
		 */
        public Builder title(String title) {
         mFilmItem.mTitle = title;
         return this;
        }

         /**
          * Sets the films user-visible byline (e.g. author and date).
          */
         public Builder byline(String byline) {
             mFilmItem.mByline = byline;
             return this;
         }

         // A hex color value that will be used for the highlight banner if no Uri is supplied
         public Builder brandingHighlightColor(String color) {
             mFilmItem.mBrandingHighlightBG = color;
             return this;
         }

         // Text to be overlaid on the highlight banner
         public Builder brandingText(String text) {
             mFilmItem.mBrandingText = text;
             return this;
         }

         // Timestamp for the FilmItem
         public Builder timestamp(long timestamp) {
             mFilmItem.mTimestamp = timestamp;
             return this;
         }

         /**
          * Sets the films opaque application-specific identifier.
          */
         public Builder token(String token) {
             mFilmItem.mToken = token;
             return this;
         }

         /**
          * Sets the activity {@link Intent} that will be
          * {@linkplain Context#startActivity(Intent) started} when
          * the user clicks for more details about the artwork.
          *
          * <p> The activity that this intent resolves to must have <code>android:exported</code>
          * set to <code>true</code>.
          *
          * <p> Because film objects can be persisted across device reboots,
          * {@linkplain android.app.PendingIntent pending intents}, which would alleviate the
          * exported requirement, are not currently supported.
          */
         public Builder viewIntent(Intent viewIntent) {
             mFilmItem.mViewIntent = viewIntent;
             return this;
         }

         /**
          * Creates and returns the final FilmItem object. Once this method is called, it is not valid
          * to further use this {@link FilmItem.Builder} object.
          */
         public FilmItem build() {
             return mFilmItem;
         }
	}

	/**
	 * Serializes this film object to a {@link Bundle} representation.
	 */
	public Bundle toBundle() {
		Bundle bundle = new Bundle();
		bundle.putString(KEY_CONTENT_URI, (mContentUri != null) ? mContentUri.toString() : null);
		bundle.putString(KEY_THUMB_URI, (mThumbUri != null) ? mThumbUri.toString() : null);
		bundle.putString(KEY_BRANDING_PHOTO_URI, (mBrandingPhotoUri != null) ? mBrandingPhotoUri.toString() : null);
		bundle.putString(KEY_BRANDING_HIGHLIGHT_IMAGE_URI, (mBrandingHighlightImageUri != null) ? mBrandingHighlightImageUri.toString() : null);
		bundle.putString(KEY_BRANDING_TAG_URI, (mBrandingTagUri != null) ? mBrandingTagUri.toString() : null);
		bundle.putString(KEY_TITLE, mTitle);
		bundle.putString(KEY_BYLINE, mByline);
		bundle.putString(KEY_BRANDING_HIGHLIGHT_BG, (mBrandingHighlightBG != null) ? mBrandingHighlightBG : null);
		bundle.putString(KEY_BRANDING_TEXT, (mBrandingText != null) ? mBrandingText : null);
		bundle.putLong(KEY_TIMESTAMP, mTimestamp);
		bundle.putString(KEY_TOKEN, mToken);
		bundle.putString(KEY_VIEW_INTENT, (mViewIntent != null)
				? mViewIntent.toUri(Intent.URI_INTENT_SCHEME) : null);
		return bundle;
	}

	/**
	 * Deserializes an film object from a {@link Bundle}.
	 */
	public static FilmItem fromBundle(Bundle bundle) {
		Builder builder = new Builder()
		.title(bundle.getString(KEY_TITLE))
		.byline(bundle.getString(KEY_BYLINE))
		.token(bundle.getString(KEY_TOKEN))
		.timestamp(bundle.getLong(KEY_TIMESTAMP));

		String contentUri = bundle.getString(KEY_CONTENT_URI);
		if (!TextUtils.isEmpty(contentUri)) {
			builder.contentUri(Uri.parse(contentUri));
		}

		String thumbUri = bundle.getString(KEY_THUMB_URI);
		if (!TextUtils.isEmpty(thumbUri)) {
			builder.thumbUri(Uri.parse(thumbUri));
		}

		String brandingPhotoUri = bundle.getString(KEY_BRANDING_PHOTO_URI);
		if (!TextUtils.isEmpty(brandingPhotoUri)) {
			builder.brandingPhotoUri(Uri.parse(brandingPhotoUri));
		}

		String brandingTagUri = bundle.getString(KEY_BRANDING_TAG_URI);
		if (!TextUtils.isEmpty(brandingTagUri)) {
			builder.brandingTagUri(Uri.parse(brandingTagUri));
		}

		String brandingHighlightImageUri = bundle.getString(KEY_BRANDING_HIGHLIGHT_IMAGE_URI);
		if (!TextUtils.isEmpty(brandingHighlightImageUri)) {
			builder.brandingHighlightImageUri(Uri.parse(brandingHighlightImageUri));
		}

		String brandingHighlightColor = bundle.getString(KEY_BRANDING_HIGHLIGHT_BG);
		if (!TextUtils.isEmpty(brandingHighlightColor)) {
			builder.brandingHighlightColor(brandingHighlightColor);
		}
		
		String brandingText = bundle.getString(KEY_BRANDING_TEXT);
		if (!TextUtils.isEmpty(brandingText)) {
			builder.brandingText(brandingText);
		}
		
		try {
			String viewIntent = bundle.getString(KEY_VIEW_INTENT);
			if (!TextUtils.isEmpty(viewIntent)) {
				builder.viewIntent(Intent.parseUri(viewIntent, Intent.URI_INTENT_SCHEME));
			}
		} catch (URISyntaxException ignored) {
		}

		return builder.build();
	}

	/**
	 * Serializes this film object to a {@link JSONObject} representation.
	 */
	public JSONObject toJson() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(KEY_CONTENT_URI, (mContentUri != null) ? mContentUri.toString() : null);
		jsonObject.put(KEY_THUMB_URI, (mThumbUri != null) ? mThumbUri.toString() : null);
		jsonObject.put(KEY_BRANDING_PHOTO_URI, (mBrandingPhotoUri != null) ? mBrandingPhotoUri.toString() : null);
		jsonObject.put(KEY_BRANDING_TAG_URI, (mBrandingTagUri != null) ? mBrandingTagUri.toString() : null);
		jsonObject.put(KEY_BRANDING_HIGHLIGHT_IMAGE_URI, (mBrandingHighlightImageUri != null) ? mBrandingHighlightImageUri.toString() : null);
		jsonObject.put(KEY_TITLE, mTitle);
		jsonObject.put(KEY_BYLINE, mByline);
		jsonObject.put(KEY_BRANDING_HIGHLIGHT_BG, (mBrandingHighlightBG != null) ? mBrandingHighlightBG : null);
		jsonObject.put(KEY_BRANDING_TEXT, (mBrandingText != null) ? mBrandingText : null);
		jsonObject.put(KEY_TIMESTAMP, mTimestamp);
		jsonObject.put(KEY_TOKEN, mToken);
		jsonObject.put(KEY_VIEW_INTENT, (mViewIntent != null)
				? mViewIntent.toUri(Intent.URI_INTENT_SCHEME) : null);
		return jsonObject;
	}

	/**
	 * Deserializes an film object from a {@link JSONObject}.
	 */
	public static FilmItem fromJson(JSONObject jsonObject) throws JSONException {
		Builder builder = new Builder()
		.title(jsonObject.optString(KEY_TITLE))
		.byline(jsonObject.optString(KEY_BYLINE))
		.token(jsonObject.optString(KEY_TOKEN))
		.timestamp(jsonObject.optLong(KEY_TIMESTAMP, 0));

		String contentUri = jsonObject.optString(KEY_CONTENT_URI);
		if (!TextUtils.isEmpty(contentUri)) {
			builder.contentUri(Uri.parse(contentUri));
		}

		String thumbUri = jsonObject.optString(KEY_THUMB_URI);
		if (!TextUtils.isEmpty(thumbUri)) {
			builder.thumbUri(Uri.parse(thumbUri));
		}

		String brandingPhotoUri = jsonObject.optString(KEY_BRANDING_PHOTO_URI);
		if (!TextUtils.isEmpty(brandingPhotoUri)) {
			builder.brandingPhotoUri(Uri.parse(brandingPhotoUri));
		}
		
		String brandingTagUri = jsonObject.optString(KEY_BRANDING_TAG_URI);
		if (!TextUtils.isEmpty(brandingTagUri)) {
			builder.brandingTagUri(Uri.parse(brandingTagUri));
		}

		String brandingHighlightImageUri = jsonObject.optString(KEY_BRANDING_HIGHLIGHT_IMAGE_URI);
		if (!TextUtils.isEmpty(brandingPhotoUri)) {
			builder.brandingHighlightImageUri(Uri.parse(brandingHighlightImageUri));
		}
		
		String brandingColor = jsonObject.optString(KEY_BRANDING_HIGHLIGHT_BG);
		if (!TextUtils.isEmpty(brandingColor)) {
			builder.brandingHighlightColor(brandingColor);
		}
		
		String brandingText = jsonObject.optString(KEY_BRANDING_TEXT);
		if (!TextUtils.isEmpty(brandingText)) {
			builder.brandingText(brandingText);
		}
		
		try {
			String viewIntent = jsonObject.optString(KEY_VIEW_INTENT);
			if (!TextUtils.isEmpty(viewIntent)) {
				builder.viewIntent(Intent.parseUri(viewIntent, Intent.URI_INTENT_SCHEME));
			}
		} catch (URISyntaxException ignored) {
		}

		return builder.build();
	}
}
