/*
 * Copyright 2014 brockoli Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brockoli.android.codered.api;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A static Utility class for bundling/unbundling and serializing FilmItem lists
 *
 */

public class FilmItemList {
	private static final String KEY_FILM_ITEM_LIST = "filmItems";
	
	public static synchronized Bundle toBundle(List<FilmItem> films) {
		ArrayList<String> serialFilms = new ArrayList<String>();
		Bundle bundle = new Bundle();
		for (FilmItem film : films) {
			try {
				serialFilms.add(film.toJson().toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		bundle.putStringArrayList(KEY_FILM_ITEM_LIST, serialFilms);			
		return bundle;
	}

	public static List<FilmItem> fromBundle(Bundle bundle) {
		ArrayList<String> serialFilms = bundle.getStringArrayList(KEY_FILM_ITEM_LIST);
		List<FilmItem> films = new ArrayList<FilmItem>();
		
		for (String serialFilm : serialFilms) {
			JSONObject jsonFilm;
			try {
				jsonFilm = new JSONObject(serialFilm);
				FilmItem film = FilmItem.fromJson(jsonFilm);
				films.add(film);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return films;
	}
	
	public static JSONArray toJson(List<FilmItem> films) {
		JSONArray jsonFilms = new JSONArray();
		
		for (FilmItem film : films) {
			try {
				JSONObject jsonFilm = film.toJson();
				jsonFilms.put(jsonFilm);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return jsonFilms;
	}
	
	public static List<FilmItem> fromJson(JSONArray films) {
		List<FilmItem> filmList = new ArrayList<FilmItem>();
		
		for (int i=0; i < films.length(); i++) {
			try {
				FilmItem film = FilmItem.fromJson(films.getJSONObject(i));
				filmList.add(film);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return filmList;
	}
}
