### CodeRed Launcher Filmstrip API ###

CodeRed Launcher is a new Android TV box launcher replacement that will be shipping on an upcoming Quad core Android TV Box from [Theatre In a Box](http://www.theaterinabox.tv/)

The launcher features a customizable filmstip control for presenting content.  CodeRed will ship with several filmstrip feeds for showcasing Facebook, Youtube, Vine, Vimeo and Instagram content.  The launcher uses a familiar IntentService based subscriber/publisher pattern to allow 3rd party apps to provide their own content to the launchers filmstrip control.

This is the API that is required by your app to integrate with the CodeRed launcher.  More details about how to use this API can be found on this blog.

### How do I get set up? ###

* Clone this repo
* Import project into ADT (sorry no Android Studio yet)
* Build the project
* Add a reference from your project to this project
* - OR - drop the api jar file into your projects libs folder
* You're all set to start development!

### Who do I talk to? ###

* You can contact me at [robwoods.edbrock@gmail.com](mailto:robwoods.edbrock@gmail.com)
